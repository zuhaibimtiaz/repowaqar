//
//  RealmServices.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/26/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import RealmSwift

class RealmServices {
    private init(){}
    static let shared = RealmServices()
    
    var realm = try! Realm()
    
    func create<T:Object>(_ object: T, onCompletion:(_ status:Bool?)->Void) {
        do{
            try realm.write {
                realm.add(object)
                onCompletion(true)
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    func update<T:Object>(_ object: T, with dict: [String: Any]) {
        do{
            try realm.write {
                for (key,value) in dict {
                    object.setValue(value, forKey: key)
                }
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    func delete<T:Object>(_ object: T) {
        do{
            try realm.write {
                realm.delete(object)
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    
    func isChasisExists(chasis: String) -> Bool {
        var find: Bool = true
        var allBikes:Results<BikeModel>!
        allBikes = realm.objects(BikeModel.self)
        for bike in allBikes {
            if chasis == bike.chasisNo {
                find = false
            }
        }
        return find
    }
}

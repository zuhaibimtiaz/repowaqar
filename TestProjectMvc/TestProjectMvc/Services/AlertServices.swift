//
//  AlertServices.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/4/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import UIKit

class AlertServices {
    
    private init (){}
    
   static func sellBikeAlert(in vc: UIViewController, completion: @escaping (String,String,String,String)->Void) {
        let alert =  UIAlertController(title: "Add Details", message: nil, preferredStyle: .alert)
        alert.addTextField { (buyerTF) in
            buyerTF.placeholder = "Enter Buyer Name"
        }
        alert.addTextField { (docTF) in
            docTF.placeholder = "Enter Document Status"
        }
        alert.addTextField { (SellPriceTF) in
            SellPriceTF.placeholder = "Enter Sold Price"
        }
        alert.addTextField { (detailTF) in
            detailTF.placeholder = "Enter Details"
        }
        let action = UIAlertAction(title: "sold", style: .default) { (_) in
            guard let buyer = alert.textFields?.first?.text,
                let docStatus = alert.textFields?[1].text,
                let soldPrice = alert.textFields?[2].text,
                let detail = alert.textFields?.last?.text
                else{ return }
            completion(buyer,docStatus,soldPrice,detail)
        }
    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (_) in
        vc.dismiss(animated: false, completion: nil)
    }
    alert.addAction(cancelAction)
    alert.addAction(action)
    vc.present(alert, animated: true, completion: nil)
    }
}

//
//  Alert+Extension.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/4/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    static func alert(title: String, message: String, VC: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: {(action) in
//             let addNewVC =  UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNewBikeViewController") as! AddNewBikeViewController
//            if VC == addNewVC {
//                VC.txtSelectMaker.text?.removeAll()
//                VC.txtSelectEnginePower.text?.removeAll()
//                self.txtSelectColor.text?.removeAll()
//                self.txtRegNo.text?.removeAll()
//                self.txtModelYear.text?.removeAll()
//                self.txtChasisNo.text?.removeAll()
//                self.txtAmount.text?.removeAll()
//                self.txtSellerName.text?.removeAll()
//                self.txtDetails.text?.removeAll()
//                self.dismiss(animated: true, completion: nil)
            //print("yahò",addNewVC)
//            }
        })
        alert.addAction(action)
        VC.present(alert, animated: true, completion: nil)
    }
}


//
//  ViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/26/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    //MARK: - Properties
    var users:Results<User>!
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first)
        let realm = RealmServices.shared.realm
        users = realm.objects(User.self)
    }
    
    
    //MARK: - Functions
    
    
    //MARK: - IBActions
    @IBAction func btnLogin(_ sender: UIButton) {
        guard let username = txtUsername.text,let password = txtPassword.text else { return }
        let realm = RealmServices.shared.realm
        let predicate = NSPredicate(format: PredicateField.findUsername, username)
        let user = realm.objects(User.self).filter(predicate)
        for each in user {
            if each.username == username {
                if each.password == password {
                    UserDefaults.standard.set(username, forKey: Identifier.checkUsername)
                    let mainVC = self.storyboard?.instantiateViewController(withIdentifier: Identifier.mainTabBarController) as! MainTabBarController
                    self.present(mainVC, animated: true, completion: nil)
                    //                    break
                }
            }
        }
        if UserDefaults.standard.value(forKey: Identifier.checkUsername) == nil {
            UIAlertController.alert(title: AlertField.loginFailedTitle, message: AlertField.loginFailedMessage, VC: self)
        }
    }
}


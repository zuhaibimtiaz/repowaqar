//
//  SellUIViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/27/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit
import RealmSwift

class SellUIViewController: UIViewController {
    
    //MARK: - Properties
    var bikeDetails: Results<BikeModel>?
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - ViewController Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = RealmServices.shared.realm
        bikeDetails = realm.objects(BikeModel.self).filter(PredicateField.documentFilter)
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    //MARK: - IBActions
    @IBAction func btnAddNewClicked(_ sender: Any) {
        let addNewVC = self.storyboard?.instantiateViewController(withIdentifier: Identifier.addNewBikeViewController) as! AddNewBikeViewController
        self.navigationController?.pushViewController(addNewVC, animated: true)
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: Identifier.checkUsername)
        let vc = storyboard?.instantiateViewController(withIdentifier: Identifiers.viewController.rawValue)
        let tabVC = UINavigationController(rootViewController: vc!)
        let share = UIApplication.shared.delegate as? AppDelegate
        share?.window?.rootViewController = tabVC
        share?.window?.makeKeyAndVisible()
    }
}

//MARK: Tableview delegate and dataSource
extension SellUIViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bikeDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.sellTableViewCell.rawValue) as! SellTableViewCell
        cell.bike = bikeDetails?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // code for to sell bike using alertServices
        let selectedBike =  bikeDetails?[indexPath.row]
        AlertServices.sellBikeAlert(in: self) { (buyer, docstatus, price, detail) in
            if docstatus == Identifier.comparePending1 || docstatus == Identifier.comparePending2 || docstatus == Identifier.compareOk1 || docstatus == Identifier.compareok2 {
                let soldDict = [
                    Fields.selldate: Date(),
                    Fields.buyerName:buyer,
                    Fields.documentStatus:docstatus,
                    Fields.soldPrice: price,
                    Fields.soldDetail: detail
                    ] as [String : Any]
                
                RealmServices.shared.update(selectedBike!, with: soldDict)
                self.tableView.reloadData()
            }
            else{
                UIAlertController.alert(title: AlertField.documentStatusErrorTitle, message: AlertField.documentStatusErrorMessage, VC: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let bike = bikeDetails?[indexPath.row]
        let editAction = UITableViewRowAction(style: .default, title: Identifier.editActionButton) { (action, indexPath) in
            self.updateAction(bike: bike, indexPath: indexPath)
        }
        let deletAction = UITableViewRowAction(style: .default, title: Identifier.deleteActionButton) { (action, indexPath) in
            self.deleteAction(bike: bike, indexPath: indexPath)
        }
        deletAction.backgroundColor = .red
        editAction.backgroundColor = .blue
        return [deletAction,editAction]
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let bike = bikeDetails?[indexPath.row]
            RealmServices.shared.delete(bike!)
        }
    }
    
    
    //MARK: - Functions
    private func deleteAction(bike: BikeModel?, indexPath: IndexPath) {
        RealmServices.shared.delete(bike!)
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    private func updateAction(bike: BikeModel?, indexPath: IndexPath) {
        // write code here to update bike info
        // use addnewVC to update info
        let formVC = self.storyboard?.instantiateViewController(withIdentifier: Identifier.addNewBikeViewController) as! AddNewBikeViewController
        formVC.isUpdate = true
        formVC.bikeSelectedDetail = bikeDetails?[indexPath.row]
        self.navigationController?.pushViewController(formVC, animated: false)
    }
    
    
}

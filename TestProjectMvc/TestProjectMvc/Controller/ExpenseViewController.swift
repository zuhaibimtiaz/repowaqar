//
//  ExpenseViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/5/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit

class ExpenseViewController: UIViewController {
    
    @IBOutlet weak var txtSelectExpenseType: UITextField!
    @IBOutlet weak var txtExpenseAmount: UITextField!
    @IBOutlet weak var txtExpenseDetails: UITextView!
    
    let selectType_arr = Array.selectType_arr
    
    //MARK: picker view
    let my_pickerView = UIPickerView()
    // Hold Current data
    var current_arr: [String] = []
    // Hold Current textField
    var active_textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSelectExpenseType.delegate = self
        
        my_pickerView.delegate = self
        my_pickerView.dataSource = self
        
        txtSelectExpenseType.inputView = my_pickerView
        
        createToolBar()
    }
    
    @IBAction func btnSaveExpense(_ sender: UIButton) {
        guard let type = txtSelectExpenseType.text, let amount = txtExpenseAmount.text, let details = txtExpenseDetails.text else { return }
        let newExpense = ExpenseModel(expenseType: type, expenceAmount: amount, expenceDetail: details)
        RealmServices.shared.create(newExpense) { (status) in
            if status == true {
                UIAlertController.alert(title: AlertField.expenseSaveTitle, message: AlertField.expenseSaveMessage, VC: self)
            }
        }
    }
}

extension ExpenseViewController:UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate {
    //MARK: picker view Delegate and DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return current_arr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return current_arr[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        active_textField.text = current_arr[row]
    }
    // create Tool bar
    func createToolBar() {
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        
        let btnDone = UIBarButtonItem(title: Identifier.pickerbarButtonCancel, style: .plain, target: self, action: #selector(btnCancelClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnCancel = UIBarButtonItem(title: Identifier.pickerbarButtonDone, style: .plain, target: self, action: #selector(btnDoneClicked))
        toolbar.setItems([btnDone,btnSpace,btnCancel], animated: false)
        
        txtSelectExpenseType.inputAccessoryView = toolbar
    }
    @objc func btnDoneClicked() {
        active_textField.resignFirstResponder()
    }
    @objc func btnCancelClicked() {
        active_textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        current_arr = selectType_arr
        active_textField = textField
        return true
    }
}


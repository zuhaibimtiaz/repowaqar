//
//  AddNewBikeViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/30/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit

class AddNewBikeViewController: UIViewController {
    
    @IBOutlet weak var txtSelectMaker: UITextField!
    @IBOutlet weak var txtSelectEnginePower: UITextField!
    @IBOutlet weak var txtSelectColor: UITextField!
    @IBOutlet weak var txtModelYear: UITextField!
    @IBOutlet weak var txtRegNo: UITextField!
    @IBOutlet weak var txtChasisNo: UITextField!
    @IBOutlet weak var txtSellerName: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtDetails: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK: contents for picker view
    let maker_arr = Array.maker_arr
    let enginepower_arr = Array.enginepower_arr
    let color_arr = Array.color_arr
    
    //MARK: picker view
    let my_pickerView = UIPickerView()
    // Hold Current data
    var current_arr: [String] = []
    // Hold Current textField
    var active_textField: UITextField!
    
    var isUpdate = false
    var bikeSelectedDetail: BikeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSelectMaker.delegate = self
        txtSelectColor.delegate = self
        txtSelectEnginePower.delegate = self
        
        my_pickerView.delegate = self
        my_pickerView.dataSource = self
        
        txtSelectMaker.inputView = my_pickerView
        txtSelectColor.inputView = my_pickerView
        txtSelectEnginePower.inputView = my_pickerView
        
        createToolBar()
        
        txtSelectMaker.text = bikeSelectedDetail?.maker
        txtSelectEnginePower.text = bikeSelectedDetail?.enginePower
        txtSelectColor.text = bikeSelectedDetail?.color
        txtModelYear.text = bikeSelectedDetail?.modelYear
        txtRegNo.text = bikeSelectedDetail?.regNo
        txtChasisNo.text = bikeSelectedDetail?.chasisNo
        txtSellerName.text = bikeSelectedDetail?.sellerName
        txtAmount.text = bikeSelectedDetail?.purchasePrice
        txtDetails.text = bikeSelectedDetail?.detail
    }
    override func viewWillAppear(_ animated: Bool) {
        if isUpdate {
            btnSave.setTitle(Identifier.buttonTitleUpdate, for: .normal)
        }
        else {
            btnSave.setTitle(Identifier.buttonTitleSave, for: .normal)
        }
    }
    
    //MARK: Actions
    @IBAction func btnSave(_ sender: UIButton) {
        
        guard let maker = txtSelectMaker.text, let enginepower = txtSelectEnginePower.text, let color = txtSelectColor.text, let modelyear = txtModelYear.text, let regno = txtRegNo.text, let chasisno = txtChasisNo.text, let sellername = txtSellerName.text, let amount = txtAmount.text, let detail = txtDetails.text else { return }
        
        //use dictionary here for parameters
        let upDict = [
            Fields.maker:maker,
            Fields.enginePower:enginepower,
            Fields.color:color,
            Fields.modelYear:modelyear,
            Fields.regNo:regno,
            Fields.chasisNo:chasisno,
            Fields.sellerName:sellername,
            Fields.purchasePrice:amount,
            Fields.detail:detail
        ]
        if isUpdate {
            RealmServices.shared.update(bikeSelectedDetail!, with:upDict)
            UIAlertController.alert(title: AlertField.updateBikeTitle, message: AlertField.updateBikeMessage, VC: self)
        }
        else  if RealmServices.shared.isChasisExists(chasis: chasisno) {
            let newBike = BikeModel(maker: maker, enginePower: enginepower, color: color, modelYear: modelyear, regNo: regno, chasisNo: chasisno, sellerName: sellername, purchasePrice: amount, detail: detail)
            
            RealmServices.shared.create(newBike) { (status) in
                if status == true {
                    UIAlertController.alert(title: AlertField.newBikeSaveTitle, message: AlertField.newBikeSaveMessage, VC: self)
                }
            }
        }
        else {
            UIAlertController.alert(title: AlertField.alreadyBikeChasisErrorTitle, message: AlertField.alreadyBikeChasisErrorMessage, VC: self)
        }
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

//MARK: Extension for TextField Delegate
extension AddNewBikeViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        active_textField = textField
        switch active_textField {
        case txtSelectMaker:
            current_arr = maker_arr
        case txtSelectEnginePower:
            current_arr = enginepower_arr
        case txtSelectColor:
            current_arr = color_arr
        default:
            print(Identifier.defaultMessage)
        }
        my_pickerView.reloadAllComponents()
        return true
    }
}

//MARK: Extension for PickerView Delegate and DataSource
extension AddNewBikeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    //MARK: picker view Delegate and DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return current_arr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return current_arr[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        active_textField.text = current_arr[row]
    }
    // create Tool bar
    func createToolBar() {
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        
        let btnDone = UIBarButtonItem(title: Identifier.pickerbarButtonCancel, style: .plain, target: self, action: #selector(btnCancelClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnCancel = UIBarButtonItem(title: Identifier.pickerbarButtonDone, style: .plain, target: self, action: #selector(btnDoneClicked))
        toolbar.setItems([btnDone,btnSpace,btnCancel], animated: false)
        
        txtSelectMaker.inputAccessoryView = toolbar
        txtSelectColor.inputAccessoryView = toolbar
        txtSelectEnginePower.inputAccessoryView = toolbar
    }
    @objc func btnDoneClicked() {
        active_textField.resignFirstResponder()
    }
    @objc func btnCancelClicked() {
        active_textField.resignFirstResponder()
    }
}

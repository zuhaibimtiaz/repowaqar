//
//  RegisterViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/26/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit
import RealmSwift

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
        guard let username = txtUsername.text,let password = txtPassword.text,let confirmpassword = txtConfirmPassword.text else { return }
        if password == confirmpassword {
            let newUser = User(username: username, password: password)
            RealmServices.shared.create(newUser) { (status) in
                if status == true {
                    self.navigationController?.popViewController(animated: true)
                    UIAlertController.alert(title: AlertField.userSaveTitle, message:AlertField.userSaveMessage, VC: self)
                }
                else
                {
                    UIAlertController.alert(title: AlertField.userRegistrationFailedTitle, message: AlertField.userRegistrationFailedMessage, VC: self)
                }
            }
        }
        else{
            UIAlertController.alert(title: AlertField.passwordNotMatchedTitle, message: AlertField.passwordNotMatchedMessage, VC: self)
        }
    }
}

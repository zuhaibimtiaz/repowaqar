//
//  SoldBikesViewController.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/5/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit
import RealmSwift

class SoldBikesViewController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var soldBikes:Results<BikeModel>!
    override func viewDidLoad() {
        super.viewDidLoad()
        soldBikesData()
        searchBarSetup()
    }
    private func soldBikesData(){
        let realm = RealmServices.shared.realm
        soldBikes = realm.objects(BikeModel.self).filter(PredicateField.soldBikeFilter)
    }
    private func searchBarSetup() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
    }
}

extension SoldBikesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        if searchText == "" {
            soldBikesData()
        }
        else {
            soldBikesData()
            soldBikes = soldBikes.filter("chasisNo == %@",searchText)
        }
        tableView.reloadData()
    }
}

extension SoldBikesViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return soldBikes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoldBikeCell")
        cell?.textLabel?.text = soldBikes[indexPath.row].chasisNo
        return cell!
    }
    
    
}

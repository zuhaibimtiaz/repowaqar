//
//  Constants.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/5/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation

//MARK: identifiers

enum Identifiers : String{
    case viewController = "ViewController"
    case sellTableViewCell = "SellTableViewCell"
}


struct Identifier {
    static let sellTableViewCell = "SellTableViewCell"
    static let mainTabBarController = "MainTabBarController"
    static let addNewBikeViewController = "AddNewBikeViewController"
    static let viewController = "ViewController"
    static let checkUsername = "username"
    static let editActionButton = "Edit"
    static let deleteActionButton = "Delete"
    static let buttonTitleUpdate = "Update"
    static let buttonTitleSave = "Save"
    static let pickerbarButtonDone = "Done"
    static let pickerbarButtonCancel = "Cancel"
    static let defaultMessage = "Default Case"
    static let comparePending1 = "pending"
    static let comparePending2 = "Pending"
    static let compareOk1 = "Ok"
    static let compareok2 = "ok"
}
//MARK: Fields
struct Fields {
    static let SellTableViewCell = "SellTableViewCell"
    static let maker = "maker"
    static let enginePower = "enginePower"
    static let color = "color"
    static let modelYear = "modelYear"
    static let regNo = "regNo"
    static let chasisNo = "chasisNo"
    static let sellerName = "sellerName"
    static let purchasePrice = "purchasePrice"
    static let detail = "detail"
    static let selldate = "selldate"
    static let buyerName = "buyerName"
    static let documentStatus = "documentStatus"
    static let soldPrice = "soldPrice"
    static let soldDetail = "soldDetail"
}
//MARK: Alert Fields
struct AlertField {
    static let loginFailedTitle = "Login Failed"
    static let loginFailedMessage = "Username or password is wrong!!!"
    static let userSaveTitle = "Saved"
    static let userSaveMessage = "User Successfully Registered..."
    static let userRegistrationFailedTitle = "Failed"
    static let userRegistrationFailedMessage = "Registration failed, Try again!!!"
    static let passwordNotMatchedTitle = "Mismatched"
    static let passwordNotMatchedMessage = "Password and confirm cassword is not matched!!!"
    static let documentStatusErrorTitle = "Document Status Error"
    static let documentStatusErrorMessage = "Enter Document status must be Pending or Ok "
    static let updateBikeTitle = "Updated"
    static let updateBikeMessage = "Bike is Updated Successfully!!!"
    static let alreadyBikeChasisErrorTitle = "Chasis Error"
    static let alreadyBikeChasisErrorMessage = "Bike With this chasis No Already Exists"
    static let expenseSaveTitle = "Expense Saved"
    static let expenseSaveMessage = "Expense is Added Successfully!!!"
    static let newBikeSaveTitle = "Bike Saved"
    static let newBikeSaveMessage = "New Bike is Added Successfully!!!"
    static let newBikeErrorTitle = ""
    static let newBikeErrorMessage = ""
}
//MARK: Predicates and Filters
struct PredicateField {
    static let findUsername = "username == %@"
    static let documentFilter = "documentStatus == nil"
    static let soldBikeFilter = "soldPrice != nil"
}
//MARK: Arrays
struct Array {
    static let maker_arr = ["Honda","Yamaha","Suzuki","United","Pak Hero","Metro"]
    static let enginepower_arr = ["70","100","125","150"]
    static let color_arr = ["Red","Black","Silver","Blue"]
    static let selectType_arr = ["Utility Bill","Maintainence","Repair","Others"]
    
}

// alert messages

// predicate identifier

// storyboard identifier

// userdefaults identifier

// dictionery identifiers

// array identifiers

//
//  ExpenseModel.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/5/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import RealmSwift

class ExpenseModel: Object {
    @objc dynamic var expenseType: String = ""
    @objc dynamic var expenceAmount: String = ""
    @objc dynamic var expenceDetail: String = ""
    @objc dynamic var expenceDate: Date = Date()
    
    convenience init(expenseType: String, expenceAmount: String, expenceDetail: String) {
        self.init()
        self.expenseType = expenseType
        self.expenceAmount = expenceAmount
        self.expenceDetail = expenceDetail
    }
}

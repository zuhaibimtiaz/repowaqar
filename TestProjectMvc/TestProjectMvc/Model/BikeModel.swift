//
//  BikeModel.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 10/1/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class BikeModel: Object {
    dynamic var maker: String = ""
    dynamic var enginePower: String = ""
    dynamic var color: String = ""
    dynamic var modelYear: String = ""
    dynamic var regNo: String = ""
    dynamic var chasisNo: String = ""
    dynamic var sellerName: String = ""
    dynamic var purchasePrice: String = ""
    dynamic var detail: String = ""
    dynamic var purchasedate: Date = Date()
   // Sold properties
    dynamic var selldate: Date? = nil
    dynamic var buyerName: String? = nil
    dynamic var documentStatus: String? = nil
    dynamic var soldPrice: String? = nil
    dynamic var soldDetail: String? = nil
    
   convenience init(maker: String, enginePower: String, color: String, modelYear: String, regNo: String, chasisNo: String, sellerName: String, purchasePrice: String, detail: String) {
        self.init()
        self.maker = maker
        self.enginePower = enginePower
        self.color = color
        self.modelYear = modelYear
        self.regNo = regNo
        self.chasisNo = chasisNo
        self.sellerName = sellerName
        self.purchasePrice = purchasePrice
        self.detail = detail
    }
}

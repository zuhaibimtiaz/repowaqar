//
//  User.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/26/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
   @objc dynamic var username: String = ""
   @objc dynamic var password: String = ""
    
    convenience init(username: String, password: String) {
        self.init()
        self.username = username
        self.password = password
    }
}

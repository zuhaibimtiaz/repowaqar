//
//  SellTableViewCell.swift
//  TestProjectMvc
//
//  Created by Zuhaib  Imtiaz on 9/27/19.
//  Copyright © 2019 Zuhaib  Imtiaz. All rights reserved.
//

import UIKit

class SellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblChasis: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var lblColor: UILabel!
    @IBOutlet weak var lblEnginePower: UILabel!
    @IBOutlet weak var lblModelYear: UILabel!
    @IBOutlet weak var lblMaker: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    var bike:BikeModel? {
        didSet{
            self.lblChasis.text = bike?.chasisNo
            self.lblAmount.text = bike?.purchasePrice
            self.lblMaker.text = bike?.maker
            self.lblModelYear.text = bike?.modelYear
            self.lblEnginePower.text = bike?.enginePower
            self.lblRegNo.text = bike?.regNo
            self.lblColor.text = bike?.color
            self.lblDetail.text = bike?.detail
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
